let my_message_list = document.getElementById("my_message_list")
let counter = 0


function show_success_message(message, param = "") {
    counter += 1;
    counter %= 1000;
    if (my_message_list.getElementsByClassName("my_row").length >= 4) {
        return;
    }
    if (param) {
        param = `<strong>${param}</strong>`
    }

    my_message_list.insertAdjacentHTML(
        "afterbegin",
        `
        <div class="message success_message my_row" id="alert_${counter}" role="alert">
          <div style="padding-right: 27px">
              ${message}${param}
          </div>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close" 
          style="top: 3px; border: unset; background: unset; 
            right: 8px; position: absolute; padding-left: 10px; 
            color: var(--main-success-border); font-size: 32px;"
          onclick="delete_alert('alert_${counter}')">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        `
    )
    end_message_adding()
}

function show_error_message(message, param = "") {
    counter += 1;
    counter %= 1000;
    if (my_message_list.getElementsByClassName("my_row").length >= 3) {
        return;
    }
    if (param) {
        param = `<strong>${param}</strong>`
    }
    my_message_list.insertAdjacentHTML(
        "afterbegin",
        `
        <div class="message error_message my_row" id="alert_${counter}" role="alert">
          <div style="padding-right: 27px">
              ${message}${param}
          </div>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close" 
          style="top: 3px; border: unset; background: unset; right: 8px; 
            position: absolute; padding-left: 10px; color: var(--main-error-border); 
            font-size: 32px;"
          onclick="delete_alert('alert_${counter}')">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        `
    )
    end_message_adding()
}

function end_message_adding() {
    let my_alert = document.getElementById("alert_" + counter)
    setTimeout(() => {
        my_alert.classList.add("active");
    }, 10)

    setTimeout(() => {
        my_alert.classList.remove("active");

        setTimeout(() => {
            my_alert.remove()
        }, 300)
    }, 4400)

}

function delete_alert(id) {
    let my_alert = document.getElementById(id)
    my_alert.classList.remove("active");

    setTimeout(() => {
        my_alert.remove()
    }, 300)
}
