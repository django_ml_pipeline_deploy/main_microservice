from os import path
import requests

import uvicorn
from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from pydantic import BaseModel
from starlette.responses import RedirectResponse

from config import model as trained_model
import config


app = FastAPI()

app.mount("/static", StaticFiles(directory=config.STATIC_DIR), name="static")

templates = Jinja2Templates(directory=config.TEMPLATES_DIR)


@app.post("/predict")
async def main_check(request: Request):
    data = await request.json()
    data_to_predict = list()
    data_to_predict.append(float(data.get("pregnancies", 0)))
    data_to_predict.append(float(data.get("glucose", 0)))
    data_to_predict.append(float(data.get("blood_pressure", 0)))
    data_to_predict.append(float(data.get("skin_thickness", 0)))
    data_to_predict.append(float(data.get("insulin", 0)))
    data_to_predict.append(float(data.get("bmi", 0)))
    data_to_predict.append(float(data.get("diabetes_pedigree_function", 0)))
    data_to_predict.append(float(data.get("age", 0)))

    return {"result": int(config.model.predict([data_to_predict])[0])}


@app.get("/", response_class=HTMLResponse)
async def main(request: Request):
    context = {
        "request": request,
        "zero_predict": config.ZERO_PREDICT_DATA,
        "one_predict": config.ONE_PREDICT_DATA,
        "model_exists": config.model is not None
    }
    return templates.TemplateResponse("main.html", context)


@app.post("/predict_by_microservice")
async def predict_by_microservice(request: Request):
    data = await request.json()
    response = requests.post(config.MODEL_PREDICT_URL, json=data)
    print(response)
    print(response.json())
    data = response.json()
    return {"result": int(data["result"])}


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=7000)
